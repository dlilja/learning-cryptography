val scala3Version = "3.1.0"

lazy val root = project
  .in(file("."))
  .settings(
    name := "LearningCryptography",
    version := "0.1.0-SNAPSHOT",

    scalaVersion := scala3Version,

    libraryDependencies += "org.scalatest" %% "scalatest-wordspec" % "3.2.13" % "test",
    libraryDependencies += "org.scalatest" %% "scalatest-shouldmatchers" % "3.2.13" % "test"
  )
