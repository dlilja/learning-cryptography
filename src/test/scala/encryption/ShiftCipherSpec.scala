package se.danlilja.cryptography.test.encryption

import se.danlilja.cryptography.encryption.ShiftCipher
import se.danlilja.cryptography.test.UnitSpec
import se.danlilja.cryptography.utils.exceptions._

import scala.util.Failure
import scala.util.Try

class ShiftCipherSpec extends UnitSpec {
  "A ShiftCipher with upper case ASCII alphabet and key 1" when {
    val cipher = ShiftCipher(1)

    "encrypting plaintext \"AAA\"" should {
      "match the ciphertext \"BBB\"" in {
        cipher.encrypt("AAA").success.value should be("BBB")
      }
    }
    "encrypting plaintext \"\"" should {
      "match the ciphertext \"\"" in {
        cipher.encrypt("").success.value should be("")
      }
    }
    "decrypting ciphertext \"BBB\"" should {
      "match the plaintext \"AAA\"" in {
        cipher.decrypt("BBB").success.value should be("AAA")
      }
    }
    "decrypting ciphertext \"\"" should {
      "match the plaintext \"\"" in {
        cipher.decrypt("").success.value should be("")
      }
    }
    "encrypting invalid plaintext" should {
      "produce Failure(PlaintextException)" in {
        cipher
          .encrypt("aaa")
          .failure
          .exception shouldBe a[PlaintextException]
        cipher
          .encrypt(".")
          .failure
          .exception shouldBe a[PlaintextException]
      }
    }
    "decrypting invalid ciphertext" should {
      "produce Failure(CiphertextException)" in {
        cipher
          .decrypt("bbb")
          .failure
          .exception shouldBe a[CiphertextException]
        cipher
          .decrypt(".")
          .failure
          .exception shouldBe a[CiphertextException]
      }
    }
  }
  "A ShiftCipher half the lower case ASCII alphabet and key 3" when {
    val cipher = ShiftCipher(3, ('a' to 'm').toVector)

    "encrypting plaintext \"aaa\"" should {
      "match the ciphertext \"ddd\"" in {
        cipher.encrypt("aaa").success.value should be("ddd")
      }
    }
    "encrypting plaintext \"\"" should {
      "match the ciphertext \"\"" in {
        cipher.encrypt("").success.value should be("")
      }
    }
    "decrypting ciphertext \"ddd\"" should {
      "match the plaintext \"aaa\"" in {
        cipher.decrypt("ddd").success.value should be("aaa")
      }
    }
    "decrypting ciphertext \"\"" should {
      "match the plaintext \"\"" in {
        cipher.decrypt("").success.value should be("")
      }
    }
    "encrypting invalid plaintext" should {
      "produce Failure(PlaintextException)" in {
        cipher
          .encrypt("nnn")
          .failure
          .exception shouldBe a[PlaintextException]
        cipher
          .encrypt("A")
          .failure
          .exception shouldBe a[PlaintextException]
        cipher
          .encrypt(".")
          .failure
          .exception shouldBe a[PlaintextException]
      }
    }
    "decrypting invalid ciphertext" should {
      "produce Failure(CiphertextException)" in {
        cipher
          .decrypt("qqq")
          .failure
          .exception shouldBe a[CiphertextException]
        cipher
          .decrypt("D")
          .failure
          .exception shouldBe a[CiphertextException]
        cipher
          .decrypt(".")
          .failure
          .exception shouldBe a[CiphertextException]
      }
    }
  }
  "Creating a ShiftCipher with an invalid key" which {
    "is above the allowed range of integers" should {
      "throw an EncryptionKeyException" in {
        an[EncryptionKeyException] should be thrownBy ShiftCipher(26)
      }
    }
    "is below the allowed range of integers" should {
      "throw an EncryptionKeyException" in {
        an[EncryptionKeyException] should be thrownBy ShiftCipher(-5)
      }
    }
    // Due to implicit conversion Char -> Int this fails because 'a'.toInt == 97
    "is lower case" should {
      "throw an EncryptionKeyException" in {
        an[EncryptionKeyException] should be thrownBy ShiftCipher('a')
      }
    }
    // Due to implicit conversion Char -> Int this fails because '.'.toInt == 46
    "is a period" should {
      "throw an EncryptionKeyException" in {
        an[EncryptionKeyException] should be thrownBy ShiftCipher('.')
      }
    }
  }
}
