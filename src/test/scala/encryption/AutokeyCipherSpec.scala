package se.danlilja.cryptography.test.encryption

import se.danlilja.cryptography.encryption.AutokeyCipher
import se.danlilja.cryptography.test.UnitSpec
import se.danlilja.cryptography.utils.exceptions._

import scala.util.Failure
import scala.util.Try

class AutokeyCipherSpec extends UnitSpec {
  "An AutokeyCipher with upper case ASCII alphabet and key 1" when {
    val cipher = AutokeyCipher(1)

    "encrypting plaintext \"AAA\"" should {
      "match the ciphertext \"BAA\"" in {
        cipher.encrypt("AAA").success.value should be("BAA")
      }
    }
    "encrypting plaintext \"BBB\"" should {
      "match the ciphertext \"CCC\"" in {
        cipher.encrypt("BBB").success.value should be("CCC")
      }
    }
    "encrypting plaintext \"\"" should {
      "match the ciphertext \"\"" in {
        cipher.encrypt("").success.value should be("")
      }
    }
    "decrypting ciphertext \"BBB\"" should {
      "match the plaintext \"AAA\"" in {
        cipher.decrypt("BBB").success.value should be("AAA")
      }
    }
    "decrypting ciphertext \"CDE\"" should {
      "match the plaintext \"BBB\"" in {
        cipher.decrypt("CDE").success.value should be("BBB")
      }
    }
    "decrypting ciphertext \"\"" should {
      "match the plaintext \"\"" in {
        cipher.decrypt("").success.value should be("")
      }
    }
    "encrypting invalid plaintext" should {
      "produce Failure(PlaintextException)" in {
        cipher
          .encrypt("aaa")
          .failure
          .exception shouldBe a[PlaintextException]
        cipher
          .encrypt(".")
          .failure
          .exception shouldBe a[PlaintextException]
      }
    }
    "decrypting invalid ciphertext" should {
      "produce Failure(CiphertextException)" in {
        cipher
          .decrypt("bbb")
          .failure
          .exception shouldBe a[CiphertextException]
        cipher
          .decrypt(".")
          .failure
          .exception shouldBe a[CiphertextException]
      }
    }
  }
  "A AutokeyCipher half the lower case ASCII alphabet and key 3" when {
    val cipher = AutokeyCipher(3, ('a' to 'm').toVector)

    "encrypting plaintext \"aaa\"" should {
      "match the ciphertext \"daa\"" in {
        cipher.encrypt("aaa").success.value should be("daa")
      }
    }
    "encrypting plaintext \"bbb\"" should {
      "match the ciphertext \"ecc\"" in {
        cipher.encrypt("bbb").success.value should be("ecc")
      }
    }
    "encrypting plaintext \"\"" should {
      "match the ciphertext \"\"" in {
        cipher.encrypt("").success.value should be("")
      }
    }
    "decrypting ciphertext \"ddd\"" should {
      "match the plaintext \"aaa\"" in {
        cipher.decrypt("ddd").success.value should be("aaa")
      }
    }
    "decrypting ciphertext \"efg\"" should {
      "match the plaintext \"bbb\"" in {
        cipher.decrypt("efg").success.value should be("bbb")
      }
    }
    "decrypting ciphertext \"\"" should {
      "match the plaintext \"\"" in {
        cipher.decrypt("").success.value should be("")
      }
    }
    "encrypting invalid plaintext" should {
      "produce Failure(PlaintextException)" in {
        cipher
          .encrypt("nnn")
          .failure
          .exception shouldBe a[PlaintextException]
        cipher
          .encrypt("A")
          .failure
          .exception shouldBe a[PlaintextException]
        cipher
          .encrypt(".")
          .failure
          .exception shouldBe a[PlaintextException]
      }
    }
    "decrypting invalid ciphertext" should {
      "produce Failure(CiphertextException)" in {
        cipher
          .decrypt("qqq")
          .failure
          .exception shouldBe a[CiphertextException]
        cipher
          .decrypt("D")
          .failure
          .exception shouldBe a[CiphertextException]
        cipher
          .decrypt(".")
          .failure
          .exception shouldBe a[CiphertextException]
      }
    }
  }
  "Creating a AutokeyCipher with an invalid key" which {
    "is above the allowed range of integers" should {
      "throw an EncryptionKeyException" in {
        an[EncryptionKeyException] should be thrownBy AutokeyCipher(26)
      }
    }
    "is below the allowed range of integers" should {
      "throw an EncryptionKeyException" in {
        an[EncryptionKeyException] should be thrownBy AutokeyCipher(-5)
      }
    }
    // Due to implicit conversion Char -> Int this fails because 'a'.toInt == 97
    "is lower case" should {
      "throw an EncryptionKeyException" in {
        an[EncryptionKeyException] should be thrownBy AutokeyCipher('a')
      }
    }
    // Due to implicit conversion Char -> Int this fails because '.'.toInt == 46
    "is a period" should {
      "throw an EncryptionKeyException" in {
        an[EncryptionKeyException] should be thrownBy AutokeyCipher('.')
      }
    }
  }
}
