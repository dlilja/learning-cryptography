package se.danlilja.cryptography.test.encryption

import se.danlilja.cryptography.encryption.SubstitutionCipher
import se.danlilja.cryptography.test.UnitSpec
import se.danlilja.cryptography.utils.exceptions._

import scala.util.Failure
import scala.util.Random
import scala.util.Try

class SubstitutionCipherSpec extends UnitSpec {
  "A SubstitutionCipher with upper case ASCII alphabet and chosen key" when {
    val key = ('A' to 'Z').zip(Random(12345).shuffle('A' to 'Z')).toMap
    val cipher = SubstitutionCipher(key)

    "encrypting plaintext \"ABCDEFGHIJKLMNOPQRSTUVWXYZ\"" should {
      "match the ciphertext \"DCXQLSUOVRGIKAMYNHTPEWBZFJ\"" in {
        cipher.encrypt("ABCDEFGHIJKLMNOPQRSTUVWXYZ").success.value should be(
          "DCXQLSUOVRGIKAMYNHTPEWBZFJ"
        )
      }
    }
    "encrypting plaintext \"\"" should {
      "match the ciphertext \"\"" in {
        cipher.encrypt("").success.value should be("")
      }
    }
    "decrypting ciphertext \"DCXQLSUOVRGIKAMYNHTPEWBZFJ\"" should {
      "match the plaintext \"ABCDEFGHIJKLMNOPQRSTUVWXYZ\"" in {
        cipher.decrypt("DCXQLSUOVRGIKAMYNHTPEWBZFJ").success.value should be(
          "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        )
      }
    }
    "decrypting ciphertext \"\"" should {
      "match the plaintext \"\"" in {
        cipher.decrypt("").success.value should be("")
      }
    }
    "encrypting invalid plaintext" should {
      "produce Failure(PlaintextException)" in {
        cipher
          .encrypt("aaa")
          .failure
          .exception shouldBe a[PlaintextException]
        cipher
          .encrypt(".")
          .failure
          .exception shouldBe a[PlaintextException]
      }
    }
    "decrypting invalid ciphertext" should {
      "produce Failure(CiphertextException)" in {
        cipher
          .decrypt("bbb")
          .failure
          .exception shouldBe a[CiphertextException]
        cipher
          .decrypt(".")
          .failure
          .exception shouldBe a[CiphertextException]
      }
    }
  }
  "A SubstitutionCipher with half the lower case ASCII alphabet and chosen key" when {
    val alphabet = ('a' to 'm').toVector
    val key = alphabet.zip(Random(12345).shuffle(alphabet)).toMap
    val cipher = SubstitutionCipher(key, alphabet)

    "encrypting plaintext \"abcdefghijklm\"" should {
      "match the ciphertext \"dmkfbhcaligej\"" in {
        cipher.encrypt("abcdefghijklm").success.value should be(
          "dmkfbhcaligej"
        )
      }
    }
    "encrypting plaintext \"\"" should {
      "match the ciphertext \"\"" in {
        cipher.encrypt("").success.value should be("")
      }
    }
    "decrypting ciphertext \"dmkfbhcaligej\"" should {
      "match the plaintext \"abcdefghijklm\"" in {
        cipher.decrypt("dmkfbhcaligej").success.value should be(
          "abcdefghijklm"
        )
      }
    }
    "decrypting ciphertext \"\"" should {
      "match the plaintext \"\"" in {
        cipher.decrypt("").success.value should be("")
      }
    }
    "encrypting invalid plaintext" should {
      "produce Failure(PlaintextException)" in {
        cipher
          .encrypt("AAA")
          .failure
          .exception shouldBe a[PlaintextException]
        cipher
          .encrypt(".")
          .failure
          .exception shouldBe a[PlaintextException]
      }
    }
    "decrypting invalid ciphertext" should {
      "produce Failure(CiphertextException)" in {
        cipher
          .decrypt("BBB")
          .failure
          .exception shouldBe a[CiphertextException]
        cipher
          .decrypt(".")
          .failure
          .exception shouldBe a[CiphertextException]
      }
    }
  }
  "Creating a ShiftCipher with an invalid key Map" which {
    "is empty" should {
      "throw an EncryptionKeyException" in {
        an[EncryptionKeyException] should be thrownBy SubstitutionCipher(Map())
      }
    }
    "has non-unique values" should {
      "throw an EncryptionKeyException" in {
        an[EncryptionKeyException] should be thrownBy {
          val invalidKey = ('A' to 'Z').map((_, 'A')).toMap
          SubstitutionCipher(invalidKey)
        }
      }
    }
    "doesn't cover all expected characters" should {
      "throw an EncryptionKeyException" in {
        an[EncryptionKeyException] should be thrownBy {
          val invalidKey = ('A' to 'X').zip('A' to 'X').toMap
          SubstitutionCipher(invalidKey)
        }
      }
    }
  }
}
