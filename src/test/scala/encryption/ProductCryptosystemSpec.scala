package se.danlilja.cryptography.test.encryption

import se.danlilja.cryptography.encryption.{
  ShiftCipher,
  AffineCipher,
  SubstitutionCipher,
  AutokeyCipher,
  LFSRCipher,
  ProductCryptosystem
}
import se.danlilja.cryptography.test.UnitSpec
import se.danlilja.cryptography.utils.exceptions._

import scala.util.{Try, Failure}

class ProductCryptosystemSpec extends UnitSpec {
  "The product of a shift cipher with key 3 and an affine cipher with key (5, 8)" when {
    val cipher1 = AffineCipher((5, 8))
    val cipher2 = ShiftCipher(3)
    val cipher = cipher2 * cipher1

    "encrypting plaintext \"AAA\"" should {
      "match the ciphertext \"LLL\"" in {
        cipher.encrypt("AAA").success.value should be("LLL")
      }
    }
    "encrypting plaintext \"BBB\"" should {
      "match the ciphertext \"QQQ\"" in {
        cipher.encrypt("BBB").success.value should be("QQQ")
      }
    }
    "encrypting plaintext \"EEE\"" should {
      "match the ciphertext \"FFF\"" in {
        cipher.encrypt("EEE").success.value should be("FFF")
      }
    }
    "encrypting plaintext \"\"" should {
      "match the ciphertext \"\"" in {
        cipher.encrypt("").success.value should be("")
      }
    }
    "decrypting ciphertext \"LLL\"" should {
      "match the plaintext \"AAA\"" in {
        cipher.decrypt("LLL").success.value should be("AAA")
      }
    }
    "decrypting ciphertext \"QQQ\"" should {
      "match the plaintext \"BBB\"" in {
        cipher.decrypt("QQQ").success.value should be("BBB")
      }
    }
    "decrypting ciphertext \"FFF\"" should {
      "match the plaintext \"EEE\"" in {
        cipher.decrypt("FFF").success.value should be("EEE")
      }
    }
    "decrypting ciphertext \"\"" should {
      "match the plaintext \"\"" in {
        cipher.decrypt("").success.value should be("")
      }
    }
    "encrypting invalid plaintext" should {
      "produce Failure(PlaintextException)" in {
        cipher
          .encrypt("aaa")
          .failure
          .exception shouldBe a[PlaintextException]
        cipher
          .encrypt(".")
          .failure
          .exception shouldBe a[PlaintextException]
      }
    }
    "decrypting invalid ciphertext" should {
      "produce Failure(CiphertextException)" in {
        cipher
          .decrypt("bbb")
          .failure
          .exception shouldBe a[CiphertextException]
        cipher
          .decrypt(".")
          .failure
          .exception shouldBe a[CiphertextException]
      }
    }
  }
}
