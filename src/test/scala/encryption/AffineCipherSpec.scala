package se.danlilja.cryptography.test.encryption

import se.danlilja.cryptography.encryption.AffineCipher
import se.danlilja.cryptography.test.UnitSpec
import se.danlilja.cryptography.utils.exceptions._

import scala.util.Failure
import scala.util.Try

class AffineCipherSpec extends UnitSpec {
  "An affine cipher with upper case ASCII alphabet and key (5,8)" when {
    val cipher = AffineCipher((5, 8))

    "checking integers relatively prime to the size of the alphabet" should {
      "match the IndexedSeq(1, 3, 5, 7, 9, 11, 15, 17, 19, 21, 23, 25)" in {
        cipher.relativelyPrime should be(
          IndexedSeq(1, 3, 5, 7, 9, 11, 15, 17, 19, 21, 23, 25)
        )
      }
    }
    "encrypting plaintext \"AAA\"" should {
      "match the ciphertext \"III\"" in {
        cipher.encrypt("AAA").success.value should be("III")
      }
    }
    "encrypting plaintext \"BBB\"" should {
      "match the ciphertext \"NNN\"" in {
        cipher.encrypt("BBB").success.value should be("NNN")
      }
    }
    "encrypting plaintext \"EEE\"" should {
      "match the ciphertext \"CCC\"" in {
        cipher.encrypt("EEE").success.value should be("CCC")
      }
    }
    "encrypting plaintext \"\"" should {
      "match the ciphertext \"\"" in {
        cipher.encrypt("").success.value should be("")
      }
    }
    "decrypting ciphertext \"III\"" should {
      "match the plaintext \"AAA\"" in {
        cipher.decrypt("III").success.value should be("AAA")
      }
    }
    "decrypting ciphertext \"NNN\"" should {
      "match the plaintext \"BBB\"" in {
        cipher.decrypt("NNN").success.value should be("BBB")
      }
    }
    "decrypting ciphertext \"CCC\"" should {
      "match the plaintext \"EEE\"" in {
        cipher.decrypt("CCC").success.value should be("EEE")
      }
    }
    "decrypting ciphertext \"\"" should {
      "match the plaintext \"\"" in {
        cipher.decrypt("").success.value should be("")
      }
    }
    "encrypting invalid plaintext" should {
      "produce Failure(PlaintextException)" in {
        cipher
          .encrypt("aaa")
          .failure
          .exception shouldBe a[PlaintextException]
        cipher
          .encrypt(".")
          .failure
          .exception shouldBe a[PlaintextException]
      }
    }
    "decrypting invalid ciphertext" should {
      "produce Failure(CiphertextException)" in {
        cipher
          .decrypt("bbb")
          .failure
          .exception shouldBe a[CiphertextException]
        cipher
          .decrypt(".")
          .failure
          .exception shouldBe a[CiphertextException]
      }
    }
  }
  "An affine cipher with alphabet List(a, b, c, d, e, f)" when {
    val cipher = AffineCipher((5, 1), ('a' to 'f').toVector)

    "checking integers relatively prime to the size of the alphabet" should {
      "match the IndexedSeq(1, 5)" in {
        cipher.relativelyPrime should be(IndexedSeq(1, 5))
      }
    }
  }
  "Creating an AffineCipher with an invalid key" which {
    "has multiplier above the allowed range of integers" should {
      "throw an EncryptionKeyException" in {
        an[EncryptionKeyException] should be thrownBy AffineCipher((26, 13))
      }
    }
    "has additive part above the allowed range of integers" should {
      "throw an EncryptionKeyException" in {
        an[EncryptionKeyException] should be thrownBy AffineCipher((13, 26))
      }
    }
    "has multiplier below the allowed range of integers" should {
      "throw an EncryptionKeyException" in {
        an[EncryptionKeyException] should be thrownBy AffineCipher((-5, 13))
      }
    }
    "has additive part below the allowed range of integers" should {
      "throw an EncryptionKeyException" in {
        an[EncryptionKeyException] should be thrownBy AffineCipher((13, -5))
      }
    }
    "has non-invertible multiplier" should {
      "throw an EncryptionKeyException" in {
        an[EncryptionKeyException] should be thrownBy AffineCipher((2, 13))
      }
    }
    "is lower case" should {
      "throw an EncryptionKeyException" in {
        an[EncryptionKeyException] should be thrownBy AffineCipher(('a', 13))
      }
    }
    "is a period" should {
      "throw an EncryptionKeyException" in {
        an[EncryptionKeyException] should be thrownBy AffineCipher((13, '.'))
      }
    }
  }
}
