package se.danlilja.cryptography.test.encryption

import se.danlilja.cryptography.encryption.LFSRCipher
import se.danlilja.cryptography.test.UnitSpec
import se.danlilja.cryptography.utils.exceptions._

import scala.util.Failure
import scala.util.Try

class LFSRCipherSpec extends UnitSpec {
  "A LFSRCipher with binary alphabet, seed key List(1, 1, 1, 1) and coefficients List(1, 0, 1, 0)" when {
    val cipher = LFSRCipher(
      key = List(1, 1, 1, 1),
      alphabet = Vector('0', '1'),
      coefficients = List(1, 0, 1, 0)
    )

    "evaluating the first 8 elements of the key stream" should {
      "match the list List(1, 1, 1, 1, 0, 0, 1, 1)" in {
        cipher.keyStream.slice(0, 8).toList should be(
          List(1, 1, 1, 1, 0, 0, 1, 1)
        )
      }
    }
    "encrypting plaintext \"01010101\"" should {
      "match the ciphertext \"10100110\"" in {
        cipher.encrypt("01010101").success.value should be("10100110")
      }
    }
    "encrypting plaintext \"111111\"" should {
      "match the ciphertext \"000011\"" in {
        cipher.encrypt("111111").success.value should be("000011")
      }
    }
    "encrypting plaintext \"\"" should {
      "match the ciphertext \"\"" in {
        cipher.encrypt("").success.value should be("")
      }
    }
    "decrypting ciphertext \"10100110\"" should {
      "match the plaintext \"01010101\"" in {
        cipher.decrypt("10100110").success.value should be("01010101")
      }
    }
    "decrypting ciphertext \"000011\"" should {
      "match the plaintext \"111111\"" in {
        cipher.decrypt("000011").success.value should be("111111")
      }
    }
    "decrypting ciphertext \"\"" should {
      "match the plaintext \"\"" in {
        cipher.decrypt("").success.value should be("")
      }
    }
    "encrypting invalid plaintext" should {
      "produce Failure(PlaintextException)" in {
        cipher
          .encrypt("aaa")
          .failure
          .exception shouldBe a[PlaintextException]
        cipher
          .encrypt(".")
          .failure
          .exception shouldBe a[PlaintextException]
        cipher
          .encrypt("2")
          .failure
          .exception shouldBe a[PlaintextException]
      }
    }
    "decrypting invalid ciphertext" should {
      "produce Failure(CiphertextException)" in {
        cipher
          .decrypt("bbb")
          .failure
          .exception shouldBe a[CiphertextException]
        cipher
          .decrypt(".")
          .failure
          .exception shouldBe a[CiphertextException]
        cipher
          .decrypt("3")
          .failure
          .exception shouldBe a[CiphertextException]
      }
    }
  }
  "A AutokeyCipher half the lower case ASCII alphabet, seed key List(1, 2, 3, 4) and coefficients List(4, 3, 2, 1)" when {
    val cipher = LFSRCipher(
      key = List(1, 2, 3, 4),
      alphabet = ('a' to 'm').toVector,
      coefficients = List(4, 3, 2, 1)
    )

    "evaluating the first 8 elements of the key stream" should {
      "match the list List(1, 2, 3, 4, 7, 6, 5, 2)" in {
        cipher.keyStream.slice(0, 8).toList should be(
          List(1, 2, 3, 4, 7, 6, 5, 2)
        )
      }
    }
    "encrypting plaintext \"aaa\"" should {
      "match the ciphertext \"bcd\"" in {
        cipher.encrypt("aaa").success.value should be("bcd")
      }
    }
    "encrypting plaintext \"ghjk\"" should {
      "match the ciphertext \"hjmb\"" in {
        cipher.encrypt("ghjk").success.value should be("hjmb")
      }
    }
    "encrypting plaintext \"\"" should {
      "match the ciphertext \"\"" in {
        cipher.encrypt("").success.value should be("")
      }
    }
    "decrypting ciphertext \"bcd\"" should {
      "match the plaintext \"aaa\"" in {
        cipher.decrypt("bcd").success.value should be("aaa")
      }
    }
    "decrypting ciphertext \"hjmb\"" should {
      "match the plaintext \"ghjk\"" in {
        cipher.decrypt("hjmb").success.value should be("ghjk")
      }
    }
    "decrypting ciphertext \"\"" should {
      "match the plaintext \"\"" in {
        cipher.decrypt("").success.value should be("")
      }
    }
    "encrypting invalid plaintext" should {
      "produce Failure(PlaintextException)" in {
        cipher
          .encrypt("nnn")
          .failure
          .exception shouldBe a[PlaintextException]
        cipher
          .encrypt("A")
          .failure
          .exception shouldBe a[PlaintextException]
        cipher
          .encrypt(".")
          .failure
          .exception shouldBe a[PlaintextException]
      }
    }
    "decrypting invalid ciphertext" should {
      "produce Failure(CiphertextException)" in {
        cipher
          .decrypt("qqq")
          .failure
          .exception shouldBe a[CiphertextException]
        cipher
          .decrypt("D")
          .failure
          .exception shouldBe a[CiphertextException]
        cipher
          .decrypt(".")
          .failure
          .exception shouldBe a[CiphertextException]
      }
    }
  }
  "Creating a AutokeyCipher with an invalid key" which {
    "is above the allowed range of integers" should {
      "throw an EncryptionKeyException" in {
        an[EncryptionKeyException] should be thrownBy LFSRCipher(key =
          List(0, 1, 2)
        )
      }
    }
    "is below the allowed range of integers" should {
      "throw an EncryptionKeyException" in {
        an[EncryptionKeyException] should be thrownBy LFSRCipher(key =
          List(0, -3)
        )
      }
    }
    // Due to implicit conversion Char -> Int this fails because 'a'.toInt == 97
    "is lower case" should {
      "throw an EncryptionKeyException" in {
        an[EncryptionKeyException] should be thrownBy LFSRCipher(List('a'))
      }
    }
    // Due to implicit conversion Char -> Int this fails because '.'.toInt == 46
    "is a period" should {
      "throw an EncryptionKeyException" in {
        an[EncryptionKeyException] should be thrownBy LFSRCipher(List('.'))
      }
    }
  }
}
