package se.danlilja.cryptography.test

import org.scalatest.wordspec.AnyWordSpec
import org.scalatest.TryValues
import org.scalatest.matchers.should.Matchers

abstract class UnitSpec extends AnyWordSpec with TryValues with Matchers
