package se.danlilja.cryptography.test.utils.functions

import se.danlilja.cryptography.test.UnitSpec
import se.danlilja.cryptography.utils.functions.ModularArithmetic._

class ModularArithmeticSpec extends UnitSpec {
  def give = afterWord("give")
  "Computing extendedEuclideanAlgorithm" when {
    "a == 5 and b == 0" should give {
      val result = extendedEuclideanAlgorithm(5, 0)
      "gcd(5,0) == 5" in {
        result("gcd") should be(5)
      }
      "first Bézout coefficient == 1" in {
        result("s") should be(1)
      }
      "second Bézout coefficient == 0" in {
        result("t") should be(0)
      }
    }
    "a == 240 and b == 46" should give {
      val result = extendedEuclideanAlgorithm(240, 46)
      "gcd(240,46) == 2" in {
        result("gcd") should be(2)
      }
      "first Bézout coefficient == -9" in {
        result("s") should be(-9)
      }
      "second Bézout coefficient == 47" in {
        result("t") should be(47)
      }
    }
    "a == 46 and b == 240" should give {
      val result = extendedEuclideanAlgorithm(46, 240)
      "gcd(46,240) == 2" in {
        result("gcd") should be(2)
      }
      "first Bézout coefficient == 47" in {
        result("s") should be(47)
      }
      "second Bézout coefficient == -9" in {
        result("t") should be(-9)
      }
    }
  }
}
