package se.danlilja.cryptography.encryption

import se.danlilja.cryptography.utils.exceptions._
import se.danlilja.cryptography.utils.functions.ModularArithmetic._

import scala.math.abs
import scala.util.Failure
import scala.util.Random
import scala.util.Success
import scala.util.Try

/** Defines an affine cipher by extending the
  * [[se.danlilja.cryptography.encryption.Cryptosystem]] trait.
  *
  * It can be used to encrypt and decrypt strings consisting of upper case ASCII
  * characters by default or any other provided alphabet. The key is a pair of
  * integers `(m, n)` where `m` and the size of the alphabet need to be
  * relatively prime and needs to be provided when creating an instance.
  * Instances are created using a companion object.
  *
  * @constructor
  *   create a new affine cipher
  * @param key
  *   encryption key to use for encrypting and decrypting
  */
case class AffineCipher(key: (Int, Int), alphabet: Vector[Char])
    extends Cryptosystem[(Int, Int)] {

  val modInverse = {
    extendedEuclideanAlgorithm(key._1, alphabet.size)("s")
  }

  val relativelyPrime = (1 to alphabet.size).filter {
    extendedEuclideanAlgorithm(_, alphabet.size)("gcd") == 1
  }

  /** Returns true if the given encryption key is valid.
    *
    * A valid encryption key needs to be a pair of integers `(m, n)` where `m`
    * and the size of `alphabet` need to be relatively prime.
    *
    * @param key
    *   encryption key to check the validity of
    */
  def keyspace(key: (Int, Int)): Boolean = {
    relativelyPrime.contains(key._1) &&
      (0 to alphabet.size - 1).contains(key._1) &&
      (0 to alphabet.size - 1).contains(key._2)
  }

  /** Returns the encrypted ciphertext of the provided plaintext wrapped in a
    * Try.
    *
    * The plaintext needs to be valid, i.e., conform to the rules provided by
    * the plaintexts method of the class. If it does not it will return a
    * Failure wrapping a PlaintextException.
    *
    * @param plaintext
    *   plaintext to encrypt
    * @return
    *   encrypted ciphertext
    */
  def encrypt(plaintext: String): Try[String] = {
    val validPlaintext: Boolean = plaintexts(plaintext)
    if (!validPlaintext) {
      Failure(PlaintextException("Plaintext is invalid"))
    } else {
      val ciphertext =
        plaintext.map { x =>
          val oldIndex = alphabet.indexOf(x)
          val newIndex = (key._1 * oldIndex + key._2) % alphabet.size
          alphabet(newIndex)
        }.mkString
      Success(ciphertext)
    }
  }

  /** Returns the decrypted plaintext of the provided ciphertext wrapped in a
    * Try.
    *
    * The ciphertext needs to be valid, i.e., conform to the rules provided by
    * the ciphertexts method of the class. If it does not it will return a
    * Failure wrapping a CiphertextException.
    *
    * @param ciphertext
    *   ciphertext to decrypt
    * @return
    *   decrypted plaintext
    */
  def decrypt(ciphertext: String): Try[String] = {
    val validCiphertext: Boolean = ciphertexts(ciphertext)
    if (!validCiphertext) {
      Failure(CiphertextException("Ciphertext is invalid"))
    } else {
      val plaintext =
        ciphertext.map { x =>
          val oldIndex = alphabet.indexOf(x)
          val newIndex = {
            val index = (modInverse * (oldIndex + (alphabet.size - key._2)))
            // making sure the index is positive
            ((index % alphabet.size) + alphabet.size) % alphabet.size
          }
          alphabet(newIndex)
        }.mkString
      Success(plaintext)
    }
  }
}

/** Factory for [[se.danlilja.cryptography.encryption.AffineCipher]] instances.
  */
object AffineCipher {

  /** Creates an affine cipher with a given encryption key and a given alphabet.
    *
    * The encryption key is a pair of integers `(m, n)` where `m` and the size
    * of the alphabet need to be relatively prime.
    *
    * @param key
    *   encryption key of the affine cipher
    * @param alphabet
    *   alphabet of the affine cipher
    */
  def apply(key: (Int, Int), alphabet: Vector[Char]): AffineCipher = {
    val cipher = new AffineCipher(key, alphabet)
    if (!(alphabet == alphabet.distinct)) {
      throw AlphabetException(
        "Alphabet entries are not distinct."
      )
    }
    if (!cipher.keyspace(key)) {
      throw EncryptionKeyException(
        "Provided encryption key is not a valid key for this AffineCipher."
      )
    }
    cipher
  }

  /** Creates an affine cipher with a given encryption key and upper case ASCII
    * alphabet.
    *
    * @param key
    *   encryption key of the affine cipher
    */
  def apply(key: (Int, Int)): AffineCipher = {
    val alphabet = ('A' to 'Z').toVector
    apply(key, alphabet)
  }

  /** Creates an affine cipher with a randomly selected encryption key and upper
    * case ASCII alphabet.
    */
  def apply(): AffineCipher = {
    val relativelyPrime = List(1, 3, 5, 7, 9, 11, 15, 17, 19, 21, 23, 25)
    val r = Random()
    val key = (r.shuffle(relativelyPrime).head, r.between(0, 25))
    apply(key)
  }
}
