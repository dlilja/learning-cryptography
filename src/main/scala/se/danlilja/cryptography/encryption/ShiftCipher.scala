package se.danlilja.cryptography.encryption

import se.danlilja.cryptography.utils.exceptions._

import scala.util.Failure
import scala.util.Random
import scala.util.Success
import scala.util.Try

/** Defines a shift cipher by extending the
  * [[se.danlilja.cryptography.encryption.Cryptosystem]] trait.
  *
  * Can be used to encrypt or decrypt strings consisting of upper case ASCII
  * characters by default or any other provided alphabet. The key is an Int
  * giving the shift. Instances are created using a companion object.
  *
  * @constructor
  *   create a new shift cipher with a given key
  * @param key
  *   encryption key of the shift cipher
  * @param alphabet
  *   alphabet of the shift cipher
  */
case class ShiftCipher(key: Int, alphabet: Vector[Char])
    extends Cryptosystem[Int] {

  /** Returns true if the given encryption key is valid.
    *
    * @param key
    *   encryption key to check the validity of
    */
  def keyspace(key: Int): Boolean = (0 to alphabet.size - 1).contains(key)

  /** Returns the encrypted ciphertext of the provided plaintext wrapped in a
    * Try.
    *
    * The plaintext needs to be valid, i.e., conform to the rules provided by
    * the plaintexts method of the class. If it does not it will return a
    * Failure wrapping a PlaintextException.
    *
    * @param plaintext
    *   plaintext to encrypt
    * @return
    *   encrypted ciphertext
    */
  def encrypt(plaintext: String): Try[String] = {
    val validPlaintext: Boolean = plaintexts(plaintext)
    if (!validPlaintext) {
      Failure(PlaintextException("Plaintext is invalid"))
    } else {
      val ciphertext =
        plaintext.map { x =>
          val oldIndex = alphabet.indexOf(x)
          val newIndex = (oldIndex + key) % alphabet.size
          alphabet(newIndex)
        }.mkString
      Success(ciphertext)
    }
  }

  /** Returns the decrypted plaintext of the provided ciphertext wrapped in a
    * Try.
    *
    * The ciphertext needs to be valid, i.e., conform to the rules provided by
    * the ciphertexts method of the class. If it does not it will return a
    * Failure wrapping a CiphertextException.
    *
    * @param ciphertext
    *   ciphertext to decrypt
    * @return
    *   decrypted plaintext
    */
  def decrypt(ciphertext: String): Try[String] = {
    val validCiphertext: Boolean = ciphertexts(ciphertext)
    if (!validCiphertext) {
      Failure(CiphertextException("Ciphertext is invalid"))
    } else {
      val plaintext =
        ciphertext.map { x =>
          val oldIndex = alphabet.indexOf(x)
          val newIndex = {
            val index = (oldIndex + (alphabet.size - key))
            // making sure the index is positive
            ((index % alphabet.size) + alphabet.size) % alphabet.size
          }
          alphabet(newIndex)
        }.mkString
      Success(plaintext)
    }
  }
}

/** Factory for [[se.danlilja.cryptography.encryption.ShiftCipher]] instances.
  */
object ShiftCipher {

  /** Creates a shift cipher with a given encryption key and a given alphabet.
    *
    * @param key
    *   encryption key of the shift cipher
    * @param alphabet
    *   alphabet of the shift cipher
    */
  def apply(key: Int, alphabet: Vector[Char]): ShiftCipher = {
    val cipher = new ShiftCipher(key, alphabet)
    if (!(alphabet == alphabet.distinct)) {
      throw AlphabetException(
        "Alphabet entries are not distinct."
      )
    }
    if (!cipher.keyspace(key)) {
      throw EncryptionKeyException(
        "Provided encryption key is not a valid key for this ShiftCipher."
      )
    }
    cipher
  }

  /** Creates a shift cipher with a given encryption key and an upper case ASCII
    * alphabet.
    *
    * @param key
    *   encryption key of the shift cipher
    */
  def apply(key: Int): ShiftCipher = {
    val alphabet = ('A' to 'Z').toVector
    apply(key, alphabet)
  }

  /** Creates a shift cipher with a randomly selected encryption key */
  def apply(): ShiftCipher = {
    val key = Random.nextInt(26)
    apply(key)
  }
}
