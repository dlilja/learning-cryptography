package se.danlilja.cryptography.encryption

import scala.util.{Try, Success, Failure}
import se.danlilja.cryptography.utils.exceptions.AlphabetException

/** Defines the abstract notion of a Cryptosystem.
  *
  *   - key: String representing the encryption key used for encryption and
  *     decryption.
  *   - alphabet: Function to determine if a specific `Char` is allowed for use
  *     in a plaintext or ciphertext string.
  *   - plaintexts: Function to determine if a string is a valid plaintext.
  *   - ciphertexts: Function to determine if a string is a valid ciphertext.
  *   - keyspace: Function to determine if a string is a valid key.
  *   - encrypt: Function taking a plaintext and a key and producing the
  *     corresponding ciphertext. Should be the inverse of decrypt.
  *   - decrypt: Function taking a ciphertext and a key and producing the
  *     corresponding plaintext. Should be the inverse of encrypt.
  */
trait Cryptosystem[T] {
  val key: T
  val alphabet: Vector[Char]

  def plaintexts(plaintext: String): Boolean = {
    plaintext.forall(alphabet.contains(_))
  }
  def ciphertexts(ciphertext: String): Boolean = {
    ciphertext.forall(alphabet.contains(_))
  }
  def keyspace(key: T): Boolean
  def encrypt(plaintext: String): Try[String]
  def decrypt(ciphertext: String): Try[String]

  // Note: This product is the reverse of what I have seen in books. This
  // version acts like function composition in the sense that if C1 and C2 are
  // cryptosystems with encryption functions E1 and E2 respectively then C1 * C2
  // has encryption function E1 o E2 where (f o g)(x) = f(g(x)).
  def *[S](other: Cryptosystem[S]): ProductCryptosystem[T, S] = {
    ProductCryptosystem(this, other)
  }
}
