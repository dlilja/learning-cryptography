package se.danlilja.cryptography.encryption

import se.danlilja.cryptography.utils.exceptions._

import scala.util.Failure
import scala.util.Random
import scala.util.Success
import scala.util.Try

/** Defines an autokey cipher by extending the
  * [[se.danlilja.cryptography.encryption.Cryptosystem]] trait.
  *
  * Can be used to encrypt or decrypt strings consisting of upper case ASCII
  * characters by default or any other provided alphabet. The key is an Int
  * giving the priming key. Instances are created using a companion object.
  *
  * @constructor
  *   create a new autokey cipher with a given priming key
  * @param key
  *   priming key of the autokey cipher
  * @param alphabet
  *   alphabet of the autokey cipher
  */
case class AutokeyCipher(key: Int, alphabet: Vector[Char])
    extends Cryptosystem[Int] {

  /** Returns true if the given priming key is valid.
    *
    * @param key
    *   encryption key to check the validity of
    */
  def keyspace(key: Int): Boolean = (0 to alphabet.size - 1).contains(key)

  /** Returns the encrypted ciphertext of the provided plaintext wrapped in a
    * Try.
    *
    * The plaintext needs to be valid, i.e., conform to the rules provided by
    * the plaintexts method of the class. If it does not it will return a
    * Failure wrapping a PlaintextException.
    *
    * @param plaintext
    *   plaintext to encrypt
    * @return
    *   encrypted ciphertext
    */
  def encrypt(plaintext: String): Try[String] = {
    val validPlaintext: Boolean = plaintexts(plaintext)
    if (!validPlaintext) {
      Failure(PlaintextException("Plaintext is invalid"))
    } else {
      val ciphertext = plaintext
        .foldLeft((key, "")) { (pair, c) =>
          val oldIndex = alphabet.indexOf(c)
          val newIndex = (oldIndex + pair._1) % alphabet.size
          (oldIndex, pair._2 + alphabet(newIndex))
        }
        ._2
      Success(ciphertext)
    }
  }

  /** Returns the decrypted plaintext of the provided ciphertext wrapped in a
    * Try.
    *
    * The ciphertext needs to be valid, i.e., conform to the rules provided by
    * the ciphertexts method of the class. If it does not it will return a
    * Failure wrapping a CiphertextException.
    *
    * @param ciphertext
    *   ciphertext to decrypt
    * @return
    *   decrypted plaintext
    */
  def decrypt(ciphertext: String): Try[String] = {
    val validCiphertext: Boolean = ciphertexts(ciphertext)
    if (!validCiphertext) {
      Failure(CiphertextException("Ciphertext is invalid"))
    } else {
      val plaintext = ciphertext
        .foldLeft((key), "") { (pair, c) =>
          val oldIndex = alphabet.indexOf(c)
          val newIndex = {
            val index = (oldIndex + (alphabet.size - pair._1))
            // maing sure the index is positive
            ((index % alphabet.size) + alphabet.size) % alphabet.size
          }
          (oldIndex, pair._2 + alphabet(newIndex))
        }
        ._2
      Success(plaintext)
    }
  }
}

/** Factory for [[se.danlilja.cryptography.encryption.AutokeyCipher]] instances.
  */
object AutokeyCipher {

  /** Creates an autokey cipher with a given encryption key and a given
    * alphabet.
    *
    * @param key
    *   encryption key of the autokey cipher
    * @param alphabet
    *   alphabet of the autokey cipher
    */
  def apply(key: Int, alphabet: Vector[Char]): AutokeyCipher = {
    val cipher = new AutokeyCipher(key, alphabet)
    if (!(alphabet == alphabet.distinct)) {
      throw AlphabetException(
        "Alphabet entries are not distinct."
      )
    }
    if (!cipher.keyspace(key)) {
      throw EncryptionKeyException(
        "Provided encryption key is not a valid key for this AutokeyCipher."
      )
    }
    cipher

  }

  /** Creates an autokey cipher with a given encryption key and an upper case
    * ASCII alphabet.
    *
    * @param key
    *   encryption key of the autokey cipher
    */
  def apply(key: Int): AutokeyCipher = {
    val alphabet = ('A' to 'Z').toVector
    apply(key, alphabet)
  }

  /** Creates an autokey cipher with a randomly selected encryption key */
  def apply(): AutokeyCipher = {
    val key = Random.nextInt(26)
    apply(key)
  }
}
