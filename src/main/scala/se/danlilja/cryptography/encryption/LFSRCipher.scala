package se.danlilja.cryptography.encryption

import se.danlilja.cryptography.utils.exceptions._

import scala.util.Failure
import scala.util.Random
import scala.util.Success
import scala.util.Try

/** Defines an LFSR cipher by extending the
  * [[se.danlilja.cryptography.encryption.Cryptosystem]] trait.
  *
  * Can be used to encrypt or decrypt strings consisting of 0 and 1 by default
  * or any other provided alphabet. The key is a List[Int] giving the seed key
  * for the key stream. Instances are created using a companion object.
  * Instances should always be created with a constructor to ensure that the
  * seed key is valid for the chosen alphabet and coefficients.
  *
  * @constructor
  *   create a new LFSR cipher with a given seed key, alphabet and coefficients
  * @param key
  *   seed key of the LFSR cipher
  * @param alphabet
  *   alphabet of the LFSR cipher
  * @param coefficients
  *   coefficients of the linear combination used to create the key stream
  */
case class LFSRCipher(
    key: List[Int],
    alphabet: Vector[Char],
    coefficients: List[Int]
) extends Cryptosystem[List[Int]] {

  /** Returns true if the given seed key is valid.
    *
    * @param key
    *   encryption key to check the validity of
    */
  def keyspace(key: List[Int]): Boolean = {
    val validSize = key.size == coefficients.size
    val validKeys = key.forall((0 to alphabet.size - 1).contains(_))
    validSize && validKeys
  }

  val keyStream: LazyList[Int] = {
    key ++: LazyList.unfold(key)(l =>
      val zipped = l.zip(coefficients)
      val newKey =
        zipped.foldLeft(0)((sum, pair) =>
          (sum + pair._1 * pair._2)
        ) % alphabet.size
      val newList = l.tail :+ newKey
      Some(newKey, newList)
    )
  }

  /** Returns the encrypted ciphertext of the provided plaintext wrapped in a
    * Try.
    *
    * The plaintext needs to be valid, i.e., conform to the rules provided by
    * the plaintexts method of the class. If it does not it will return a
    * Failure wrapping a PlaintextException.
    *
    * @param plaintext
    *   plaintext to encrypt
    * @return
    *   encrypted ciphertext
    */
  def encrypt(plaintext: String): Try[String] = {
    val validPlaintext: Boolean = plaintexts(plaintext)
    if (!validPlaintext) {
      Failure(PlaintextException("Plaintext is invalid"))
    } else {
      val zippedPlaintext = plaintext.zip(keyStream)
      val ciphertext = zippedPlaintext.map { (c, s) =>
        val oldIndex = alphabet.indexOf(c)
        val newIndex = (oldIndex + s) % alphabet.size
        alphabet(newIndex)
      }.mkString
      Success(ciphertext)
    }
  }

  /** Returns the decrypted plaintext of the provided ciphertext wrapped in a
    * Try.
    *
    * The ciphertext needs to be valid, i.e., conform to the rules provided by
    * the ciphertexts method of the class. If it does not it will return a
    * Failure wrapping a CiphertextException.
    *
    * @param ciphertext
    *   ciphertext to decrypt
    * @return
    *   decrypted plaintext
    */
  def decrypt(ciphertext: String): Try[String] = {
    val validCiphertext: Boolean = ciphertexts(ciphertext)
    if (!validCiphertext) {
      Failure(CiphertextException("Ciphertext is invalid"))
    } else {
      val zippedCiphertext = ciphertext.zip(keyStream)
      val plaintext = zippedCiphertext.map { (c, s) =>
        val oldIndex = alphabet.indexOf(c)
        val newIndex = (oldIndex + (alphabet.size - s)) % alphabet.size
        alphabet(newIndex)
      }.mkString
      Success(plaintext)
    }
  }
}

/** Factory for [[se.danlilja.cryptography.encryption.LFSRCipher]] instances.
  */
object LFSRCipher {

  /** Creates an LFSR cipher with a given encryption key, a given alphabet and
    * given coefficients.
    *
    * @param key
    *   encryption key of the LFSR cipher
    * @param alphabet
    *   alphabet of the LFSR cipher
    * @param coefficients
    *   coefficients of the linear combination used to create the key stream
    */
  def apply(
      key: List[Int],
      alphabet: Vector[Char],
      coefficients: List[Int]
  ): LFSRCipher = {
    val cipher = new LFSRCipher(key, alphabet, coefficients)
    if (!(alphabet == alphabet.distinct)) {
      throw AlphabetException(
        "Alphabet entries are not distinct."
      )
    }
    if (!cipher.keyspace(key)) {
      throw EncryptionKeyException(
        "Provided seed key is not a valid LFSRCipher seed key"
      )
    }
    cipher
  }

  /** Creates an LFSR cipher with a given encryption key, a given alphabet and
    * randomly selected coefficients.
    *
    * @param key
    *   encryption key of the LFSR cipher
    * @param alphabet
    *   alphabet of the LFSR cipher
    */
  def apply(key: List[Int], alphabet: Vector[Char]): LFSRCipher = {
    val coefficients =
      List.fill(key.size)(Random().between(0, alphabet.size))
    apply(key, alphabet, coefficients)
  }

  /** Creates an LFSR cipher with a given seed key, a binary alphabet and
    * randomly selected coefficients.
    *
    * @param key
    *   encryption key of the LFSR cipher
    */
  def apply(key: List[Int]): LFSRCipher = {
    val alphabet = Vector('0', '1')
    apply(key, alphabet)
  }

  /** Creates an LFSR cipher with a randomly selected seed key of size 256,
    * binary alphabet, and randomly selected coefficients.
    */
  def apply(): LFSRCipher = {
    val key = List.fill(256)(Random().between(0, 2))
    apply(key)
  }
}
