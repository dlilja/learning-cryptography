package se.danlilja.cryptography.encryption

import se.danlilja.cryptography.utils.exceptions._

import scala.util.Failure
import scala.util.Random
import scala.util.Success
import scala.util.Try

/** Defines a substitution cipher by extending the
  * [[se.danlilja.cryptography.encryption.Cryptosystem]] trait.
  *
  * By default it can be used to encrypt or decrypt strings consisting of upper
  * case ASCII characters. The key is a permutation of the upper case ASCII
  * characters in the form of a Map[Char, Char]. The key can either be provided
  * explicitly or selected randomly depending on the constructor used. Instances
  * are created using a companion object.
  *
  * @constructor
  *   create a new substitution cipher
  * @param key
  *   encryption key to use for encrypting and decrypting
  * @param alphabet
  *   the alphabet of allowed characters in plaintext and ciphertext strings
  */
case class SubstitutionCipher(key: Map[Char, Char], alphabet: Vector[Char])
    extends Cryptosystem[Map[Char, Char]] {

  /** Returns true if the given encryption key is valid.
    *
    * A valid encryption key needs to be a permutation of the upper case ASCII
    * characters in the form of a Map[Char, Char].
    *
    * @param key
    *   encryption key to check the validity of
    */
  def keyspace(key: Map[Char, Char]): Boolean = {
    val validSize =
      key.keySet.size == alphabet.size && key.values.toSet.size == alphabet.size
    val validChars =
      key.forall((k, v) => alphabet.contains(k) && alphabet.contains(v))
    validSize && validChars
  }

  /** Returns the encrypted ciphertext of the provided plaintext wrapped in a
    * Try.
    *
    * The plaintext needs to be valid, i.e., conform to the rules provided by
    * the plaintexts method of the class. If it does not it will return a
    * Failure wrapping a PlaintextException.
    *
    * @param plaintext
    *   plaintext to encrypt
    * @return
    *   encrypted ciphertext
    */
  def encrypt(plaintext: String): Try[String] = {
    val validPlaintext: Boolean = plaintexts(plaintext)
    if (!validPlaintext) {
      Failure(PlaintextException("Plaintext is invalid"))
    } else {
      val ciphertext = plaintext.map(key(_))
      Success(ciphertext)
    }
  }

  /** Returns the decrypted plaintext of the provided ciphertext wrapped in a
    * Try.
    *
    * The ciphertext needs to be valid, i.e., conform to the rules provided by
    * the ciphertexts method of the class. If it does not it will return a
    * Failure wrapping a CiphertextException.
    *
    * @param ciphertext
    *   ciphertext to decrypt
    * @return
    *   decrypted plaintext
    */
  def decrypt(ciphertext: String): Try[String] = {
    val inverseKey = key.map(_.swap)
    val validCiphertext: Boolean = ciphertexts(ciphertext)
    if (!validCiphertext) {
      Failure(CiphertextException("Ciphertext is invalid"))
    } else {
      val plaintext = ciphertext.map(inverseKey(_))
      Success(plaintext)
    }
  }
}

/** Factory for [[se.danlilja.cryptography.encryption.SubstitutionCipher]]
  * instances.
  */
object SubstitutionCipher {

  /** Creates a substitution cipher with a given encryption key and a given
    * alphabet.
    *
    * @param key
    *   encryption key of the substitution cipher
    * @param alphabet
    *   alphabet of the substitution cipher
    */
  def apply(
      key: Map[Char, Char],
      alphabet: Vector[Char]
  ): SubstitutionCipher = {
    val cipher = new SubstitutionCipher(key, alphabet)
    if (!(alphabet == alphabet.distinct)) {
      throw AlphabetException(
        "Alphabet entries are not distinct."
      )
    }
    if (!cipher.keyspace(key)) {
      throw EncryptionKeyException(
        "Provided encryption key is not a valid key for this SubstitutionCipher."
      )
    }
    cipher
  }

  /** Creates a substitution cipher with a given encryption key and upper case
    * ASCII alphabet.
    *
    * @param key
    *   encryption key of the substitution cipher
    */
  def apply(key: Map[Char, Char]): SubstitutionCipher = {
    val alphabet = ('A' to 'Z').toVector
    apply(key, alphabet)
  }

  /** Creates a substitution cipher with a randomly selected encryption key and
    * upper case ASCII alphabet.
    */
  def apply(): SubstitutionCipher = {
    val key = ('A' to 'Z').zip(Random.shuffle('A' to 'Z')).toMap
    apply(key)
  }
}
