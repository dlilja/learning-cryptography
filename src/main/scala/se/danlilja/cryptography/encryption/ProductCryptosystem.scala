package se.danlilja.cryptography.encryption

import scala.util.{Try, Success, Failure}
import se.danlilja.cryptography.utils.exceptions.AlphabetException
import scala.annotation.constructorOnly

/** Defines the ProductCryptosystem class which is an extension of the
  * [[se.danlilja.cryptography.encryption.Cryptosystem]] trait.
  *
  * It can be used to create more complicated cryptosystem starting from simple
  * cryptosystems.
  *
  * @constructor
  *   create a new product cryptosystem
  * @param left
  *   the left factor of the cryptosystem product
  * @param right
  *   the right factor of the cryptosystem product
  */
class ProductCryptosystem[S, T](left: Cryptosystem[S], right: Cryptosystem[T])
    extends Cryptosystem[(S, T)] {

  val key = (left.key, right.key)
  val alphabet = left.alphabet

  def keyspace(key: (S, T)): Boolean = {
    left.keyspace(key._1) && right.keyspace(key._2)
  }

  def encrypt(plaintext: String): Try[String] = {
    right.encrypt(plaintext).flatMap(left.encrypt)
  }

  def decrypt(ciphertext: String): Try[String] = {
    left.decrypt(ciphertext).flatMap(right.decrypt)
  }

  // Note: This product is the reverse of what I have seen in books. This
  // version acts like function composition in the sense that if C1 and C2 are
  // cryptosystems with encryption functions E1 and E2 respectively then C1 * C2
  // has encryption function E1 o E2 where (f o g)(x) = f(g(x)).
}

object ProductCryptosystem {

  /** Creates a ProductCryptosystem from two existing cryptosystems.
    */
  def apply[S, T](
      left: Cryptosystem[S],
      right: Cryptosystem[T]
  ): ProductCryptosystem[S, T] = {
    if (left.alphabet != right.alphabet) {
      throw AlphabetException("The alphabets do not match")
    }
    new ProductCryptosystem(left, right)
  }
}
