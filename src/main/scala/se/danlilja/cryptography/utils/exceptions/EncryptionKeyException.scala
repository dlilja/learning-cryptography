package se.danlilja.cryptography.utils.exceptions

case class EncryptionKeyException(
  message: String
) extends RuntimeException(message)
