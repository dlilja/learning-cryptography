package se.danlilja.cryptography.utils.exceptions

case class CiphertextException(
  message: String
) extends RuntimeException(message)
