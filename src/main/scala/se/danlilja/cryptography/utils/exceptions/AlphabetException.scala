package se.danlilja.cryptography.utils.exceptions

case class AlphabetException(
  message: String
) extends RuntimeException(message)

