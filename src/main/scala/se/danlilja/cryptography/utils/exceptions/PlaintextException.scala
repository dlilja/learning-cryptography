package se.danlilja.cryptography.utils.exceptions

case class PlaintextException(
  message: String
) extends RuntimeException(message)
