package se.danlilja.cryptography.utils.random

trait PseudoRandomNumberGenerator {
  val seed: Int

  def nextInt(): Int
  def nextInts(): Iterable[Int]
}
