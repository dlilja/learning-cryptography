package se.danlilja.cryptography.utils.random

case class LinearCongruentialGenerator(seed: Int, modulus: Int)
    extends PseudoRandomNumberGenerator {
  def nextInt(): Int = ???
  def nextInts(): Vector[Int] = ???
}
