package se.danlilja.cryptography.utils.functions

object ModularArithmetic {

  /** Uses the extended Euclidean algorithm to compute the greatest common
    * divisor of two integers `a >= 0` and `b >= 0` and the coefficient's of
    * Bézout's identity.
    *
    * @param a
    *   first integer
    * @param b
    *   second integer
    * @return
    *   map of `String` -> `Int` where key `"gcd"` gives the greatest common
    *   divisor, key `"s"` gives the Bézout coefficient of `a` and key `"t"`
    *   gives the Bézout coefficient of `b`.
    */
  def extendedEuclideanAlgorithm(
      a: Int,
      b: Int,
      sPair: (Int, Int) = (1, 0),
      tPair: (Int, Int) = (0, 1)
  ): Map[String, Int] = {
    if (b == 0) {
      Map(
        "gcd" -> a,
        "s" -> sPair._1,
        "t" -> tPair._1
      )
    } else {
      val q = a / b
      val r = a % b
      val news = sPair._1 - q * sPair._2
      val newt = tPair._1 - q * tPair._2
      extendedEuclideanAlgorithm(b, r, (sPair._2, news), (tPair._2, newt))
    }
  }
}
