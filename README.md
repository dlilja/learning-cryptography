# Learning Cryptography with Scala #

## Description ##

This project is a Scala library for cryptography. The purpose of this library is
strictly for the author to learn the fundamentals of cryptography and is not
optimized for performance in any way and there are no guarantees on the security
or correctness of any code contained within.

## Copyright ##

Copright 2022 Dan Lilja

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
